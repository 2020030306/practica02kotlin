package com.example.practica02kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private var btnIMC: Button? = null
    private var btnClean: Button? = null
    private var btnClose: Button? = null
    private var txtAltura: EditText? = null
    private var txtPeso: EditText? = null
    private var lblIMC: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnIMC = findViewById<View>(R.id.btnCalcular) as Button
        txtAltura = findViewById<View>(R.id.txtAltura) as EditText
        txtPeso = findViewById<View>(R.id.txtPeso) as EditText
        lblIMC = findViewById<View>(R.id.lblIMC) as TextView
        btnClean = findViewById<View>(R.id.btnLimpiar) as Button
        btnClose = findViewById<View>(R.id.btnCerrar) as Button

        btnIMC!!.setOnClickListener {
            if (txtAltura!!.text.toString() == "" || txtPeso!!.text.toString() == "") {
                Toast.makeText(this@MainActivity, "Faltó capturar información", Toast.LENGTH_SHORT)
                    .show()
            } else {
                val alt = txtAltura!!.text.toString()
                val pes = txtPeso!!.text.toString()
                val altu = alt.toFloat()
                val peso = pes.toFloat()
                val altur = altu * altu
                val calculo = peso / altur
                val str = "Tu IMC es: $calculo Kg/m^2"
                lblIMC!!.text = str
            }
        }

        btnClean!!.setOnClickListener {
            txtAltura!!.setText("")
            txtPeso!!.setText("")
            lblIMC!!.text = ""
        }

        btnClose!!.setOnClickListener { finish() }
    }
}